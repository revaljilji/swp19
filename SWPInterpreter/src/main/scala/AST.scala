sealed trait Node

case class Program(main: Node) extends Node

// TODO Add case classes to represent the AST.
case class Integer(num: Int) extends Node
case class Braces(brace: Node) extends Node
case class Li(list: Any) extends Node
case class Bool(boolean: Boolean) extends Node
case class RecAccess(n: Node, str: String) extends Node
case class recordDef(list: Any) extends Node
case class Function(list: Any, n: Node) extends Node
case class Variable(id: String) extends Node
case class RecDef(list: Any) extends Node
case class Cond(e1: Node, e2: Node, e3: Node) extends Node
case class Call(e: Node, list: Any) extends Node