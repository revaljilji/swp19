import scala.util.parsing.combinator._

class ExpParser extends JavaTokenParsers {

  // TODO Implement the expression parser and add additional parsers for terminal and non terminal symbols, where necessary.
  def int: Parser[Integer] = "0|[1-9][0-9]*|-[1-9][0-9]*".r ^^ {num => Integer(num.toInt)}
  def expression: Parser[Node] = int | braces | cond | variable | list | call
  val id: Parser[String] = "[a-z]([A-Z]|[a-z]|[0-9]|\\?|_)*".r

  def program: Parser[Program] = expression ^^ {
    expr => Program(expr)
  }

  def braces: Parser[Braces] = "(" ~expression~ ")" ^^ {
    case _~e~_ => Braces(e)
  }

  def cond: Parser[Cond] = "if" ~ expression ~ "then" ~ expression ~ "else" ~ expression ^^ {
    case _~e1~_~e2~_~e3 => Cond(e1,e2,e3)
  }

  def variable: Parser[Variable] = id ^^ {
    id => Variable(id)
  }

  def list: Parser[Li] = "[" ~ repsep(expression, ",") ~ "]" ^^ {
    case _~e~_ => Li(e)
  }
  
  def call: Parser[Call] = expression ~ "(" ~ repsep(expression, ",") ~ ")" ^^ {
    case e~_~l~_ => Call(e,l)
  }

  /* def expression: Parser[Node] = braces | cond | call | list | variable | bool | int | recordAccess | recordDef | function
  def braces: Parser[Braces] = "(" ~expression~ ")" ^^ {
    case _~e~_ => Braces(e)
  }
  def list: Parser[Li] = "[" ~ repsep(expression, ",") ~ "]" ^^ {
    case _~e~_ => Li(e)
  }
  def variable: Parser[Variable] = id ^^ {
    id => Variable(id)
  }
  def recordAccess: Parser[RecAccess] = expression ~ "." ~ id ^^ {
    case e~_~i => RecAccess(e, i)
  }
  def function: Parser[Function] = "(" ~ repsep(id, ",") ~ ")" ~ "->" ~ expression ^^ {
    case _~i~_~_~e => Function(i, e)
  }
  def bool: Parser[Bool] = "False|True".r ^^ {
    b => Bool(b.toBoolean)
  }
  def recordDef: Parser[RecDef] = "obj" ~ "{" ~ rep(id~":"~>expression~ ";") ~ "}" ^^ {
    case _~_~i~_ => RecDef(i)
  }
  
  def cond: Parser[Cond] = "if" ~ expression ~ "then" ~ expression ~ "else" ~ expression ^^ {
    case _~e1~_~e2~_~e3 => Cond(e1,e2,e3)
  }
  def call: Parser[Call] = expression ~ "(" ~ repsep(expression, ",") ~ ")" ^^ {
    case e~_~l~_ => Call(e,l)
  } */
} 

object ParseProgram extends ExpParser {
  def parse(s: String): ParseResult[Program] = {
    parseAll(program, s)
  }
}