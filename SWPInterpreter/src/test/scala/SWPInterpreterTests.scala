import org.scalatest.FunSuite
import scala.util.parsing.combinator._
import scala.collection.mutable._

class SWPInterpreterTests extends FunSuite {

  // TODO write your own testcases if you want and feel free to share them in the newsgroup!

  def expectValidGrammar(prog: String) = {
    if(!SWPInterpreter.checkProgramGrammar(prog)) {
      fail(SWPInterpreter.checkProgramGrammarStringResult(prog))
    }
  }

  def expectInvalidGrammar(prog: String) = {
    assert(!SWPInterpreter.checkProgramGrammar(prog))
  }

  def expectResult(prog: String, expect: ExpValue) = {
    assertResult(EvaluationResultSuccess(expect, "")) {
      SWPInterpreter.evaluateProgram(prog, new Queue())
    }
  }

  def expectResult(prog: String, expectedResult: ExpValue, input: Queue[String], expectedOutput: String) = {
    assertResult(EvaluationResultSuccess(expectedResult, expectedOutput)) {
      SWPInterpreter.evaluateProgram(prog, input)
    }
  }

  test("Int") {
    expectValidGrammar("""
      42
    """)
  }

  test("Braces") {
    expectValidGrammar("""
      (((42)))
    """)
  }

  test("Cond") {
    expectValidGrammar("""
      if 42 then ((42)) else (42)
    """)
  }

  test("Variable") {
    expectValidGrammar("""
      if 42 then (varrrriririri16) else (42)
    """)
  }

  test("List") {
    expectValidGrammar("""
      [(asd1asd?_)]
    """)
  }

  /*
  test("Parser short program") {
    expectValidGrammar("""
      first([if eq?(True, False) then obj { a: 5; }.a else 0, (x) -> add(x, 2)])
    """)
  }

  test("Parser defect program") {
    expectInvalidGrammar("""
      obj {
        min: 05;
        clamp: (num) -> ((min2) -> if lt?(num, min2) min2 else num)(min,);
        result: clamp(3());
      }.result
    """)
  }

  test("Interpreter program with only built in functions") {
    expectResult("""
      if eq?([1], build(1, [])) then add(3, sub(-2, -1)) else add(4, 2)
    """,
    ExpInteger(2))
  }

  test("Interpreter basic closures") {
    expectResult("""
      first([() -> (a, b) -> add(a, sub(a, b))])()(3, 2)
    """,
    ExpInteger(4))
  }

  test("Interpreter variable capturing") {
    expectResult("""
      first(obj {
        a: 2;
        b: (b) -> [() -> add(a, b)];
        a: 4;
      }.b(3))()
    """,
    ExpInteger(7))
  }

  test("Interpreter records") {
    expectResult("""
      first([obj {
        a: 1;
        b: 2;
        a: add(a, obj {
          b: add(b, b);
        }.b);
      }, False]).a
    """,
    ExpInteger(5))
  } */

  /*
  test("Interpreter string basic") {
    expectResult("""
      add(1, ": Hello World!")
    """,
    ExpString("1: Hello World!"))
  }

  test("Interpreter hello world") {
    expectResult("""
      first([0, obj { x: print("Hello"); x: print(" World!"); }])
    """,
    ExpInteger(0),
    new Queue(),
    "Hello World!")
  }

  test("Interpreter read") {
    expectResult("""
      read()
    """,
    ExpString("Echo"),
    Queue("Echo"),
    "")
  }

  test("Parser comments") {
    expectValidGrammar("""
      add(#)
      1, 2)##
      # 0
    """)
  }

  test("Parser comments invalid") {
    expectInvalidGrammar("""
      add(1#, 2)
    """)
  }

  test("Parser multiline comments") {
    expectValidGrammar("""
      add(#*
      +#)*#1,#*
      swp*#2
      )
    """)
  }
  */
}
